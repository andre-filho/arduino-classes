const int ledverd = 12;
const int ledama = 11;
const int ledverm = 10;
const int bot1 = 4;
const int bot2 = 3;
const int bot3 = 2;
int estbot1 = 0, estbot2 = 0, estbot3 = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledverd, OUTPUT);
  pinMode(ledama, OUTPUT);
  pinMode(ledverm, OUTPUT);
  pinMode(bot1, INPUT);
  pinMode(bot2, INPUT);
  pinMode(bot3, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  estbot1 = digitalRead(bot1);
  estbot2 = digitalRead(bot2);
  estbot3 = digitalRead(bot3);

  if(estbot1 == HIGH)
  {
    digitalWrite(ledverd,HIGH);
  }
  else
    digitalWrite(ledverd,LOW);

  if(estbot2 == HIGH)
  {
    digitalWrite(ledama, HIGH);
  }
  else
    digitalWrite(ledama,LOW);

  if(estbot3 == HIGH)
  {
    digitalWrite(ledverm,HIGH);
  }
  else
    digitalWrite(ledverm,LOW);
}

#include <LiquidCrystal.h>
LiquidCrystal lcd (12,11,5,4,3,2);

void setup() {
  // put your setup code here, to run once:
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  lcd.print("Explosion in:");
  lcd.setCursor(0,1);
  for(int x = 60; x >= 0; x--)
  {
    if(x > 9)
    {
      lcd.print(x);
      lcd.print(" seconds.");
      delay(1000);
      lcd.setCursor(0,1);
    }else{
      lcd.setCursor(0,1);
      lcd.print(0);
      lcd.print(x);
      lcd.print(" seconds.");
      delay(1000);
      lcd.setCursor(0,1);
    }
    if(x == 0)
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("XABLAU!!!!");
      delay(2000);
      lcd.setCursor(0,1);
      lcd.print("Game Over !");
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}

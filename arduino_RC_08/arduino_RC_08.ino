#define led 11
const int potenc = A0;

int v_potenc = 0;
int pwm = 0;

void setup() {
  // put your setup code here, to run once:
pinMode(led,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  v_potenc = analogRead(potenc);
  pwm = map(v_potenc, 0, 1023, 0, 255);
  analogWrite(led, pwm);
}

const int potenciometro = A0;
const int led = 13;
int vpot = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(led,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  vpot = analogRead(potenciometro);
  digitalWrite(led,HIGH);
  delay(vpot);
  digitalWrite(led,LOW);
  delay(vpot);
}

const int led[] = {5,6,7,8,9,10,11};
const int lm35 = 1;
const int ldr = 0;
const int buzzer = 2;
int valor_ldr = 0, adclido = 0, pwm = 0;
float temperatura = 0;


void setup() {
  // put your setup code here, to run once:
  for(int x = 0; x < 7; x++)
  {
    pinMode(led[x], OUTPUT);
  }
  pinMode(buzzer, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  valor_ldr = analogRead(ldr);
  adclido = analogRead(lm35);
  temperatura = adclido * 0.4887585532;

  if(temperatura > 20)
  {
    digitalWrite(led[0], HIGH);
  }else
    digitalWrite(led[0], HIGH);

  if(temperatura > 25)
  {
    digitalWrite(led[1], HIGH);    
  }else
    digitalWrite(led[1], LOW);

  if(temperatura > 30)
  {
    digitalWrite(led[2], HIGH);
  }else
    digitalWrite(led[2], HIGH);

  if (valor_ldr > 500)
  {     digitalWrite(led[5], HIGH);   
  }   else{
    digitalWrite(led[5], LOW);   }
    if (valor_ldr > 400)
    {     digitalWrite(led[4], HIGH);   }
    else{     digitalWrite(led[4], LOW);   }
    if (valor_ldr > 350)
    {     digitalWrite(led[3], HIGH);  
    digitalWrite(led[6], LOW);    
    digitalWrite(buzzer, LOW);    
    }   else{     digitalWrite(led[3], LOW);    
    digitalWrite(led[6], HIGH);    
    digitalWrite(buzzer, HIGH);  
    }
    } 

#include <LiquidCrystal.h>
LiquidCrystal lcd (12,11,5,4,3,2);

void setup() {
  // put your setup code here, to run once:
  lcd.begin(16,2);
  lcd.setCursor(0,0);
    lcd.print("Explosion in");
    for(int x=60;x>=0;x--)
    {
      lcd.setCursor(0,1);
      if(x<10)
      {
        lcd.print(0);
        lcd.print(x);
        lcd.print(" seconds.");
      }else
      {
        lcd.print(x);
        lcd.print(" seconds.");
      }
      if(x==0)
      {
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("BOOOOOM!!!");
        lcd.setCursor(0,1);
        delay(1000);
        lcd.print("Game Over...");
      }
      delay(1000);
    }
}

void loop() {
  // put your main code here, to run repeatedly:
    
}

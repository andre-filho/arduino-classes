const int LM35 = 0;
const int buzzer = 12;
const int led[] = {2,3,4,5,6,7,8,9,10,11};
float temp = 0;
int ADClido = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  analogReference(INTERNAL);
  pinMode(buzzer, OUTPUT);
  for(int x=0;x<10;x++){
    pinMode(led[x],OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  ADClido = analogRead(LM35);
  temp = ADClido * 0.1075268817;
  
  if(temp > 26){
    digitalWrite(led[0],HIGH);
  }
  else
  digitalWrite(led[0],LOW);

  if(temp > 27){
    digitalWrite(led[1],HIGH);
  }
  else
  digitalWrite(led[1],LOW);

  if(temp > 28){
    digitalWrite(led[2],HIGH);
  }
  else
  digitalWrite(led[2],LOW);

  if(temp > 29){
    digitalWrite(led[3],HIGH);
  }
  else
  digitalWrite(led[3],LOW);

  if(temp > 29.3){
    digitalWrite(led[4],HIGH);
  }
  else
  digitalWrite(led[4],LOW);

  if(temp > 29.6){
    digitalWrite(led[5],HIGH);
  }
  else
  digitalWrite(led[5],LOW);

  if(temp > 29.8){
    digitalWrite(led[6],HIGH);
  }
  else
  digitalWrite(led[6],LOW);

  if(temp > 29.9){
    digitalWrite(led[7],HIGH);
  }
  else
  digitalWrite(led[7],LOW);

  if(temp > 30.1){
    digitalWrite(led[8],HIGH);
  }
  else
  digitalWrite(led[8],LOW);

  if(temp > 32){
    digitalWrite(led[9],HIGH);
    digitalWrite(buzzer,HIGH);
  }
  else{
  digitalWrite(led[9],LOW);
  digitalWrite(buzzer,LOW);
  }
}

const int r = 9;
const int g = 10;
const int b = 11;

int valorred = 255, valorgreen = 0, valorblue = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(r,OUTPUT);
  pinMode(g,OUTPUT);
  pinMode(b,OUTPUT);
  analogWrite(r,valorred);
  analogWrite(g,valorgreen);
  analogWrite(b,valorblue);
}

void loop() {
  // put your main code here, to run repeatedly:
  for(valorgreen = 0; valorgreen <255; valorgreen += 5)
  {
    analogWrite(g,valorgreen);
    delay(50);
  }

  for(valorred = 255; valorred >0; valorred -= 5)
  {
    analogWrite(r,valorred);
    delay(50);
  }

  for(valorblue = 0; valorblue<255; valorblue += 5)
  {
    analogWrite(b,valorblue);
    delay(50);
  }


  for(valorgreen = 255; valorgreen >0; valorgreen -= 5)
  {
    analogWrite(g,valorgreen);
    delay(50);
  }

  for(valorred = 0; valorred <255; valorred += 5)
  {
    analogWrite(r,valorred);
    delay(50);
  }

  for(valorblue = 255; valorblue>0; valorblue -= 5)
  {
    analogWrite(b,valorblue);
    delay(50);
  }
}

const int LDR = 0;
int valor_lido = 0;
const int LED = 6;
int pwm = 0;

void setup() {
  // put your setup code here, to run once:
  //Serial.begin(9600);
  pinMode(LED, OUTPUT);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  valor_lido = analogRead(LDR);
  //Serial.print("LDR = ");
  //Serial.println(valor_lido);
  if(valor_lido < 500)
  {
    analogWrite(LED, pwm);
    pwm++;
    delay(100);
  }
  else{
    digitalWrite(LED, LOW);
    pwm = 0;
  }
  if(pwm > 255)
    pwm = 255;
}

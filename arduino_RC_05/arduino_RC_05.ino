const int LM35 = 0;
const int buzzer = 12;
float temperatura = 0;
int ADClido = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  analogReference(INTERNAL);
  pinMode(buzzer, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  ADClido = analogRead(LM35);
  temperatura = ADClido * 0.1075268817;
  
  if(temperatura > 30){
    digitalWrite(buzzer,HIGH);
  }
  else
  digitalWrite(buzzer,LOW);
}

const int ledverd = 12;
const int ledama = 11;
const int ledverm = 10;
const int bot1 = 4;
const int bot2 = 3;
const int bot3 = 2;
const int buzzer = 9;
int estbot1 = 0, estbot2 = 0, estbot3 = 0;
int tom = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledverd, OUTPUT);
  pinMode(ledama, OUTPUT);
  pinMode(ledverm, OUTPUT);
  pinMode(bot1, INPUT);
  pinMode(bot2, INPUT);
  pinMode(bot3, INPUT);
  pinMode(buzzer, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  estbot1 = digitalRead(bot1);
  estbot2 = digitalRead(bot2);
  estbot3 = digitalRead(bot3);

  if(estbot1 && !estbot2 && !estbot3)
  {
    tom = 100;
    digitalWrite(ledverd,HIGH);
  }
 
  if(estbot2 && !estbot1 && !estbot3)
  {
    tom = 200;
    digitalWrite(ledama, HIGH);
  }
  
  if(estbot3 && !estbot2 && !estbot1)
  {
    tom = 300;
    digitalWrite(ledverm,HIGH);
  }

while (tom > 0)
{
  digitalWrite(buzzer, HIGH);
  delayMicroseconds(tom);
  digitalWrite(buzzer,LOW);
  delayMicroseconds(tom);
  tom = 0;
  digitalWrite(ledverd,LOW);
  digitalWrite(ledama,LOW);
  digitalWrite(ledverm,LOW);
}
}

